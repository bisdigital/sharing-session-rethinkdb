import React from "react";
import ReactDOM from "react-dom";
import ReactRethinkdb from "react-rethinkdb";
// var ReactRethinkdb = require('react-rethinkdb');
const r = ReactRethinkdb.r;


class Content extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            drivers : [],
            nameval: '',
            // mixins: [ReactRethinkdb.DefaultMixin,]
        }
    }

    componentDidMount=()=>{
        ReactRethinkdb.DefaultSession.connect({
            host: 'localhost',
            port: 8015,
            path: '/db',
            secure: false,
            db: 'Monitoring',
            autoRecconectDelayMs: 2000,
        })
        // this.data()
    }

    // data=()=>{
    //     r.table('driver').changes().run(connection, function(err, cursor) {
    //         if (err) throw err;
    //         cursor.each(function(err, row) {
    //             if (err) throw err;
    //             console.log(JSON.stringify(row, null, 2));
    //         });
    //     });
    // }

    observe=  (props, state)=>{
        var drivers =  new ReactRethinkdb.QueryRequest({
            query: r.table('driver'),
            changes: true,
            initial: [],
        })
        console.log(drivers.data)
    };

    handleSubmit= async (e)=>{
        e.preventDefault();
        var nameval = this.refs.NameDriver;
        var query = r.table('driver').insert({
        name: nameval.value, long:'106.816666', lat:'-6.200000', status:'A'
        });
        nameval.value = '';
        ReactRethinkdb.DefaultSession.runQuery(query);
        
    }

    OnDelete= async (e)=>{
        e.preventDefault();
        // var nameval = this.refs.NameDriver;
        var query = r.table('driver').filter({
        id:'6a430ce8-7593-4fe4-b5ad-8105c8d7acf8'
        }).delete();
        ReactRethinkdb.DefaultSession.runQuery(query);
    }

    OnUpdate= async (e)=>{
        e.preventDefault();
        var nameval = this.refs.NameDriver;
        var query = r.table('driver').insert({
        name: nameval.value, long:'106.816666', lat:'-6.200000', status:'A'
        });
        nameval.value = '';
        ReactRethinkdb.DefaultSession.runQuery(query);
    }


    render() {
        
        return (
            <div className="App">
                {/* {
                    this.state.drivers.map((element)=>{
                        return(
                        <div>{element.id}</div>
                        )
                    })
                } */}
                <div className="col-md-6">
                    <form onSubmit = {this.handleSubmit} >
                    <div className="row">
                        <div className="col-md-4">Name
                        <input type="text" ref="NameDriver"></input>
                        </div>
                        <div className="col-md-4">
                        <button type="submit" >Tambah Driver</button>
                        </div>
                    </div>
                    </form>
                    <button type="submit" onClick={this.OnDelete}>Hapus Driver</button>

                </div>
            </div>
        )
    }

}

export default Content;